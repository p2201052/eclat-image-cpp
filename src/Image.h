#ifndef INC_12201052_12018792_12202776_IMAGE_H
#define INC_12201052_12018792_12202776_IMAGE_H

#include "Pixel.h"
#include <string>

/**
 * @brief La classe Image représente une image.
 */
class Image {
private:
     int dimx; ///< Dimension horizontale de l'image.
     int dimy; ///< Dimension verticale de l'image.
     Pixel *tab; ///< Tableau de pixels représentant l'image.

    /**
     * @brief Constructeur par défaut de la classe Image.
     */
public:
    Image();

    /**
     * @brief Constructeur de la classe Image avec des dimensions spécifiées.
     * @param dimx Dimension horizontale de l'image.
     * @param dimy Dimension verticale de l'image.
     */
    Image(int dimensionX, int dimensionY);

    /**
     * @brief Destructeur de la classe Image.
     */
    ~Image();

    /**
     * @brief Obtient le pixel aux coordonnées spécifiées.
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     * @return Le pixel aux coordonnées (x, y).
     */
    Pixel& getPix(int x, int y);

    /**
    * @brief Obtient le pixel aux coordonnées spécifiées.
    * @param x Coordonnée horizontale du pixel.
    * @param y Coordonnée verticale du pixel.
    * @return Le pixel aux coordonnées (x, y).
    */
    Pixel getPix (int x, int y) const ;

    /**
   * @brief Obtient La dimension x de l'image.
   * @return La dimension x de l'image.
   */
    int getDimx() const;

    /**
  * @brief Obtient La dimension y de l'image.
  * @return La dimension y de l'image.
  */
    int getDimy() const;


    /**
     * @brief Verifie si les coordonnees du pixel son valide
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     * @return true or false
     */
    bool validCoordonnee(int x, int y) const;
    /**
     * @brief Définit la couleur du pixel aux coordonnées spécifiées.
     * @param x Coordonnée horizontale du pixel.
     * @param y Coordonnée verticale du pixel.
     * @param couleur La couleur à définir pour le pixel.
     */
    void setPix(int x, int y, const Pixel& couleur);

    /**
     * @brief Dessine un rectangle dans l'image avec la couleur spécifiée.
     * @param Xmin Coordonnée horizontale minimale du rectangle.
     * @param Ymin Coordonnée verticale minimale du rectangle.
     * @param Xmax Coordonnée horizontale maximale du rectangle.
     * @param Ymax Coordonnée verticale maximale du rectangle.
     * @param couleur La couleur du rectangle.
     */
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel& couleur);

    /**
     * @brief Efface l'image en la remplissant avec la couleur spécifiée.
     * @param pixel La couleur avec laquelle effacer l'image.
     */
    void effacer(const Pixel& pixel);

    /**
     * @brief Exécute des tests de régression sur la classe Image.
     */
    static void testRegression();


    //partie 2

    //Sauvergarde de l'image
    void sauver(const std::string &filename) const;
    //ouverture de l'image
    void ouvrir(const std::string &filename);
    //affichage sur console
    void afficherConsole();
};

#endif //INC_12201052_12018792_12202776_IMAGE_H
