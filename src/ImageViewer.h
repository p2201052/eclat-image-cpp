// ImageViewer.h
#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <SDL.h>
#include "Image.h"

/**
 * @brief La classe ImageViewer permet d'afficher une image avec SDL2 dans une fenêtre.
 */
class ImageViewer {
private:
    SDL_Window* window; ///< Pointeur vers la fenêtre SDL.
    SDL_Renderer* renderer; ///< Pointeur vers le renderer SDL.

public:
    /**
     * @brief Constructeur de la classe ImageViewer.
     * Initialise SDL2 et crée une fenêtre et un renderer.
     */
    ImageViewer();

    /**
     * @brief Destructeur de la classe ImageViewer.
     * Ferme la fenêtre et libère les ressources SDL2.
     */
    ~ImageViewer();

    /**
     * @brief Affiche une image dans la fenêtre SDL.
     * @param im L'image à afficher.
     */
    void afficher(const Image& im);
};

#endif // IMAGEVIEWER_H
