#ifndef INC_12201052_12018792_12202776_PIXEL_H
#define INC_12201052_12018792_12202776_PIXEL_H
#include <iostream>
/**
 * @brief Structure représentant un pixel dans une image.
 */
struct Pixel {
    unsigned char r; ///< Composante rouge du pixel.
    unsigned char g; ///< Composante verte du pixel.
    unsigned char b; ///< Composante bleue du pixel.

    /**
     * @brief Constructeur par défaut de la structure Pixel.
     * Initialise le pixel avec les valeurs par défaut (0, 0, 0).
     */
    Pixel() : r(0), g(0), b(0) {}

    /**
     * @brief Constructeur de la structure Pixel avec des valeurs spécifiées.
     * @param nr Valeur de la composante rouge.
     * @param ng Valeur de la composante verte.
     * @param nb Valeur de la composante bleue.
     */
    Pixel(unsigned char nr, unsigned char ng, unsigned char nb) : r(nr), g(ng), b(nb) {}

    /**
     * @brief Surcharge de l'opérateur d'égalité pour comparer deux pixels.
     * @param pixel Le pixel à comparer avec le pixel actuel.
     * @return true si les pixels sont égaux, sinon false.
     */
    bool operator==(const Pixel& pixel) const ;

    /**
     * @brief Surcharge de l'opérateur de sortie pour afficher un pixel.
     * @param os Le flux de sortie.
     * @param pixel Le pixel à afficher.
     * @return Le flux de sortie mis à jour.
     */
    friend std::ostream& operator<<(std::ostream& os, const Pixel& pixel) {
        os << "(" << pixel.r << ", " << pixel.g << ", " << pixel.b << ")";
        return os;
    }

    /**
    * @brief Procedure d'affichage d'un Pixel
    * @param pix Pixel à afficher
   */
    void afficherPixel(const Pixel& pix);
};

#endif //INC_12201052_12018792_12202776_PIXEL_H
